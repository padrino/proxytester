Requirements: 

- Docker https://www.docker.com/
- terraform https://www.terraform.io/
- terraform docker provider https://releases.hashicorp.com/terraform-provider-docker

Setup: 

- Build ProxyScanner doker image 
```
 docker build -t netcrave/proxyscanner -t netcrave/proxyscanner:latest .
...
Successfully built f4eb44bd76bf
Successfully tagged netcrave/proxyscanner:latest

```

- Run `terraform plan`
```
terraform plan -out terraform.plan
```

- Review the plan then run `terraform apply`
```
 terraform apply "terraform.plan"
```

- teardown 
```
terraform destroy
```

Notes: 

https://github.com/hashicorp/terraform/issues/12095
https://github.com/hashicorp/terraform/issues?utf8=%E2%9C%93&q=label%3Aconfig+docker+

docker unix socket
https://github.com/hashicorp/terraform/issues/4923
