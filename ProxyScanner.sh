#!/bin/bash

# erratic@yourstruly.sx

mkdir -p /dev/shm/proxy_scan/{first_pass,second_pass}
RIAK_SERVER="http://127.0.0.1:8098"

### HTTP only proxy check
curl --connect-timeout 120 -m 120 \
     http://tools.rosinstrument.com/proxy/l100.xml http://tools.rosinstrument.com/proxy/plab100.xml \
    | grep "title" \
    | tr '>' ' ' \
    | tr '<' ' ' \
    | awk '{print $2}' \
    | parallel -j100 curl --connect-timeout 120 -m 120 -L --proxy {} http://microsoft.com '>' /dev/shm/proxy_scan/first_pass/{}


# HTTP_CONNECT proxy check (connects to FTP server and retrieves MOTD to verify)
cd /dev/shm/proxy_scan/first_pass/
grep -r "Microsoft Corporation" \
    | tr ':' " " \
    | awk '{print $1":"$2}' \
    | sed 's/.result//g' \
    | parallel -j100 curl --proxy {} --connect-timeout 120 -m 120 -D- ftp://ftp.oregonstate.edu ">" /dev/shm/proxy_scan/second_pass/{}

### Extract HTTP_CONNECT proxies (grep for the FTP server banner title)
cd /dev/shm/proxy_scan/second_pass/
grep -r "O S U O S L" \
    | tr ':' " " \
    | awk '{print $1":"$2}' >> /dev/shm/proxy_scan/http_connect.txt

### Extract HTTP-only proxies
cd /dev/shm/proxy_scan/first_pass/
grep -r "Microsoft Corporation" \
    | tr ':' " " \
    | awk '{print $1":"$2}' >> /dev/shm/proxy_scan/http.txt

### Store HTTP_CONNECT proxies in Riak
for proxy in $(cat /dev/shm/proxy_scan/http_connect.txt); do
    key=$(echo $proxy | md5sum | awk '{print $1}')
    proxy_host=$(echo $proxy | tr ':' '' | awk '{print $1}')
    proxy_port=$(echo $proxy | tr ':' '' | awk '{print $2}')
    query=$(cat <<EOT
{
    "proxy_host": "${proxy_host}",
    "proxy_port": "${proxy_port}",
    "http_connect": true
}
EOT
)

    curl -v \
         -XPUT \
         -d "${query}" \
         -H "Content-Type: application/json" \
         "${RIAK_SERVER}/buckets/inbound_proxies/keys/${key}"

done

### Store HTTP-only proxies in Riak
for proxy in $(cat /dev/shm/proxy_scan/http.txt); do
    key=$(echo $proxy | md5sum | awk '{print $1}')
    proxy_host=$(echo $proxy | tr ':' ' ' | awk '{print $1}')
    proxy_port=$(echo $proxy | tr ':' ' ' | awk '{print $2}')
    query=$(cat <<EOT
{
   "proxy_host": "${proxy_host}",
   "proxy_port": "${proxy_port}",
   "http_connect": false
}
EOT
)

    curl -v \
         -XPUT \
         -d "${query}" \
         -H "Content-Type: application/json" \
         "${RIAK_SERVER}/buckets/inbound_proxies/keys/${key}"

done

rm -r /dev/shm/proxy_scan

