# Configure the Docker provider

provider "docker" {
  host = "unix:///var/run/docker.sock"
}

# Define custom network
# https://www.terraform.io/docs/providers/docker/r/network.html#subnet
resource "docker_network" "NetcraveProxyScanner" {
  name = "NetcraveProxyScanner"
  check_duplicate = true
  internal = true
  ipam_config = {
    subnet = "172.16.242.0/30"
  }
}

# Create a container
# https://www.terraform.io/docs/providers/docker/r/container.html
resource "docker_container" "NetcraveProxyScannerRiak" {
  image = "${docker_image.riak.latest}"
  name  = "NetcraveProxyScannerRiak"
  hostname = "NetcraveProxyScannerRiak"
  ports = [{internal = 8087}, {internal = 8098}]
  must_run = true

}

resource "docker_container" "NetcraveProxyScanner" {
  env = ["RIAK_SERVER=${docker_container.NetcraveProxyScannerRiak.ip_address}"]
  must_run = true
  hostname = "NetcraveProxyScanner"
  name = "NetcraveProxyScanner"
  image = "netcrave/proxyscanner:latest"
}

# define docker images
# https://www.terraform.io/docs/providers/docker/r/image.html
resource "docker_image" "riak" {
  name = "basho/riak-kv:latest"
  keep_locally = true
}

# This is useful for pulling images from docker hub but this image will not
# be available on docker hub
# resource "docker_image" "NetcraveProxyScanner" {
#  name = "netcrave/proxyscanner:latest"
#  keep_locally = true
#  pull_triggers = ["f4eb44bd76bf"]
# }